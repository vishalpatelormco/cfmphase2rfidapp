﻿namespace HAAS_PC_HMI
{
    partial class CFMPhase2RFIDApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CFMPhase2RFIDApp));
            this.Retrievefile = new System.Windows.Forms.Button();
            this.PartID = new System.Windows.Forms.TextBox();
            this.ServerLocation_Text = new System.Windows.Forms.TextBox();
            this.Destinationlocation = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PartIDDefaultSuffix = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Visionsystem = new System.Windows.Forms.RadioButton();
            this.RFID = new System.Windows.Forms.RadioButton();
            this.Manual = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.IPAddress_Text = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Settingsbox = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.FileExtension = new System.Windows.Forms.TextBox();
            this.STARTTCP = new System.Windows.Forms.Button();
            this.OperatorInterface = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Responsetimetext = new System.Windows.Forms.TextBox();
            this.RFIDReadbutton = new System.Windows.Forms.Button();
            this.STATUS = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.RFIDTimer = new System.Windows.Forms.Timer(this.components);
            this.HeartbeatTimer = new System.Windows.Forms.Timer(this.components);
            this.StatusUpdate = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.Settingsbox.SuspendLayout();
            this.OperatorInterface.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Retrievefile
            // 
            this.Retrievefile.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Retrievefile.Location = new System.Drawing.Point(227, 320);
            this.Retrievefile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Retrievefile.Name = "Retrievefile";
            this.Retrievefile.Size = new System.Drawing.Size(177, 53);
            this.Retrievefile.TabIndex = 0;
            this.Retrievefile.Text = "Retrieve File";
            this.Retrievefile.UseVisualStyleBackColor = true;
            this.Retrievefile.Click += new System.EventHandler(this.Retrievefile_Click);
            // 
            // PartID
            // 
            this.PartID.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PartID.Location = new System.Drawing.Point(227, 204);
            this.PartID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PartID.Name = "PartID";
            this.PartID.Size = new System.Drawing.Size(457, 75);
            this.PartID.TabIndex = 1;
            this.PartID.TextChanged += new System.EventHandler(this.PartID_TextChanged);
            // 
            // ServerLocation_Text
            // 
            this.ServerLocation_Text.Location = new System.Drawing.Point(292, 46);
            this.ServerLocation_Text.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ServerLocation_Text.Name = "ServerLocation_Text";
            this.ServerLocation_Text.Size = new System.Drawing.Size(153, 22);
            this.ServerLocation_Text.TabIndex = 2;
            // 
            // Destinationlocation
            // 
            this.Destinationlocation.Location = new System.Drawing.Point(292, 153);
            this.Destinationlocation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Destinationlocation.Name = "Destinationlocation";
            this.Destinationlocation.Size = new System.Drawing.Size(153, 22);
            this.Destinationlocation.TabIndex = 3;
            this.Destinationlocation.Text = "C:\\Gcode";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(144, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Server Location";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(115, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Destination Location";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(27, 214);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 31);
            this.label3.TabIndex = 6;
            this.label3.Text = "PartID";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // PartIDDefaultSuffix
            // 
            this.PartIDDefaultSuffix.Location = new System.Drawing.Point(292, 102);
            this.PartIDDefaultSuffix.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PartIDDefaultSuffix.Name = "PartIDDefaultSuffix";
            this.PartIDDefaultSuffix.Size = new System.Drawing.Size(153, 22);
            this.PartIDDefaultSuffix.TabIndex = 8;
            this.PartIDDefaultSuffix.Text = "AA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(206, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Aligner and Configuration Suffix";
            // 
            // Visionsystem
            // 
            this.Visionsystem.AutoSize = true;
            this.Visionsystem.Location = new System.Drawing.Point(20, 59);
            this.Visionsystem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Visionsystem.Name = "Visionsystem";
            this.Visionsystem.Size = new System.Drawing.Size(117, 21);
            this.Visionsystem.TabIndex = 10;
            this.Visionsystem.TabStop = true;
            this.Visionsystem.Text = "Vision System";
            this.Visionsystem.UseVisualStyleBackColor = true;
            this.Visionsystem.CheckedChanged += new System.EventHandler(this.Visionsystem_CheckedChanged);
            // 
            // RFID
            // 
            this.RFID.AutoSize = true;
            this.RFID.Location = new System.Drawing.Point(337, 59);
            this.RFID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.RFID.Name = "RFID";
            this.RFID.Size = new System.Drawing.Size(60, 21);
            this.RFID.TabIndex = 11;
            this.RFID.TabStop = true;
            this.RFID.Text = "RFID";
            this.RFID.UseVisualStyleBackColor = true;
            this.RFID.CheckedChanged += new System.EventHandler(this.RFID_CheckedChanged);
            // 
            // Manual
            // 
            this.Manual.AutoSize = true;
            this.Manual.Location = new System.Drawing.Point(195, 59);
            this.Manual.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Manual.Name = "Manual";
            this.Manual.Size = new System.Drawing.Size(75, 21);
            this.Manual.TabIndex = 12;
            this.Manual.TabStop = true;
            this.Manual.Text = "Manual";
            this.Manual.UseVisualStyleBackColor = true;
            this.Manual.CheckedChanged += new System.EventHandler(this.Manual_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Visionsystem);
            this.groupBox1.Controls.Add(this.Manual);
            this.groupBox1.Controls.Add(this.RFID);
            this.groupBox1.Location = new System.Drawing.Point(29, 543);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(516, 123);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Input Method (Only One)";
            this.groupBox1.Visible = false;
            // 
            // IPAddress_Text
            // 
            this.IPAddress_Text.Location = new System.Drawing.Point(292, 212);
            this.IPAddress_Text.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.IPAddress_Text.Name = "IPAddress_Text";
            this.IPAddress_Text.Size = new System.Drawing.Size(137, 22);
            this.IPAddress_Text.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(188, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "IP Address";
            // 
            // Settingsbox
            // 
            this.Settingsbox.Controls.Add(this.label8);
            this.Settingsbox.Controls.Add(this.FileExtension);
            this.Settingsbox.Controls.Add(this.STARTTCP);
            this.Settingsbox.Controls.Add(this.Destinationlocation);
            this.Settingsbox.Controls.Add(this.label6);
            this.Settingsbox.Controls.Add(this.ServerLocation_Text);
            this.Settingsbox.Controls.Add(this.IPAddress_Text);
            this.Settingsbox.Controls.Add(this.label1);
            this.Settingsbox.Controls.Add(this.label2);
            this.Settingsbox.Controls.Add(this.label5);
            this.Settingsbox.Controls.Add(this.PartIDDefaultSuffix);
            this.Settingsbox.Location = new System.Drawing.Point(29, 124);
            this.Settingsbox.Margin = new System.Windows.Forms.Padding(4);
            this.Settingsbox.Name = "Settingsbox";
            this.Settingsbox.Padding = new System.Windows.Forms.Padding(4);
            this.Settingsbox.Size = new System.Drawing.Size(516, 412);
            this.Settingsbox.TabIndex = 16;
            this.Settingsbox.TabStop = false;
            this.Settingsbox.Text = "Settings";
            this.Settingsbox.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(169, 274);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 17);
            this.label8.TabIndex = 21;
            this.label8.Text = "File Extension";
            // 
            // FileExtension
            // 
            this.FileExtension.Location = new System.Drawing.Point(292, 271);
            this.FileExtension.Margin = new System.Windows.Forms.Padding(4);
            this.FileExtension.Name = "FileExtension";
            this.FileExtension.Size = new System.Drawing.Size(132, 22);
            this.FileExtension.TabIndex = 20;
            // 
            // STARTTCP
            // 
            this.STARTTCP.Location = new System.Drawing.Point(125, 320);
            this.STARTTCP.Margin = new System.Windows.Forms.Padding(4);
            this.STARTTCP.Name = "STARTTCP";
            this.STARTTCP.Size = new System.Drawing.Size(128, 60);
            this.STARTTCP.TabIndex = 19;
            this.STARTTCP.Text = "START TCP/IP";
            this.STARTTCP.UseVisualStyleBackColor = true;
            this.STARTTCP.Click += new System.EventHandler(this.STARTTCP_Click);
            // 
            // OperatorInterface
            // 
            this.OperatorInterface.Controls.Add(this.label10);
            this.OperatorInterface.Controls.Add(this.textBox1);
            this.OperatorInterface.Controls.Add(this.label9);
            this.OperatorInterface.Controls.Add(this.Responsetimetext);
            this.OperatorInterface.Controls.Add(this.RFIDReadbutton);
            this.OperatorInterface.Controls.Add(this.STATUS);
            this.OperatorInterface.Controls.Add(this.label7);
            this.OperatorInterface.Controls.Add(this.PartID);
            this.OperatorInterface.Controls.Add(this.label3);
            this.OperatorInterface.Controls.Add(this.Retrievefile);
            this.OperatorInterface.Location = new System.Drawing.Point(29, 118);
            this.OperatorInterface.Margin = new System.Windows.Forms.Padding(4);
            this.OperatorInterface.Name = "OperatorInterface";
            this.OperatorInterface.Padding = new System.Windows.Forms.Padding(4);
            this.OperatorInterface.Size = new System.Drawing.Size(1048, 542);
            this.OperatorInterface.TabIndex = 17;
            this.OperatorInterface.TabStop = false;
            this.OperatorInterface.Text = "Operator Interface";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(116, 410);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 17);
            this.label10.TabIndex = 26;
            this.label10.Text = "Read From PLC";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(233, 406);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(457, 22);
            this.textBox1.TabIndex = 25;
            this.textBox1.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(548, 519);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(281, 17);
            this.label9.TabIndex = 24;
            this.label9.Text = "File Transfer Response Time (Milliseconds)";
            // 
            // Responsetimetext
            // 
            this.Responsetimetext.Location = new System.Drawing.Point(831, 511);
            this.Responsetimetext.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Responsetimetext.Name = "Responsetimetext";
            this.Responsetimetext.Size = new System.Drawing.Size(100, 22);
            this.Responsetimetext.TabIndex = 23;
            // 
            // RFIDReadbutton
            // 
            this.RFIDReadbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RFIDReadbutton.Location = new System.Drawing.Point(440, 320);
            this.RFIDReadbutton.Margin = new System.Windows.Forms.Padding(4);
            this.RFIDReadbutton.Name = "RFIDReadbutton";
            this.RFIDReadbutton.Size = new System.Drawing.Size(156, 53);
            this.RFIDReadbutton.TabIndex = 22;
            this.RFIDReadbutton.Text = "RFID Reader";
            this.RFIDReadbutton.UseVisualStyleBackColor = true;
            this.RFIDReadbutton.Click += new System.EventHandler(this.RFIDReadbutton_Click);
            // 
            // STATUS
            // 
            this.STATUS.AutoSize = true;
            this.STATUS.BackColor = System.Drawing.SystemColors.Window;
            this.STATUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STATUS.Location = new System.Drawing.Point(229, 76);
            this.STATUS.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.STATUS.Name = "STATUS";
            this.STATUS.Size = new System.Drawing.Size(153, 54);
            this.STATUS.TabIndex = 1;
            this.STATUS.Text = "label8";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(27, 76);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(267, 92);
            this.label7.TabIndex = 0;
            this.label7.Text = "Status and Notifications";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // Timer1
            // 
            this.Timer1.Enabled = true;
            this.Timer1.Interval = 250;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(29, 26);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(199, 75);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1440, 698);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(181, 60);
            this.button1.TabIndex = 19;
            this.button1.Text = "EXIT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // RFIDTimer
            // 
            this.RFIDTimer.Enabled = true;
            this.RFIDTimer.Interval = 250;
            this.RFIDTimer.Tick += new System.EventHandler(this.RFIDTimer_Tick);
            // 
            // HeartbeatTimer
            // 
            this.HeartbeatTimer.Interval = 1000;
            this.HeartbeatTimer.Tick += new System.EventHandler(this.HeartbeatTimer_Tick);
            // 
            // StatusUpdate
            // 
            this.StatusUpdate.FormattingEnabled = true;
            this.StatusUpdate.ItemHeight = 16;
            this.StatusUpdate.Location = new System.Drawing.Point(1085, 124);
            this.StatusUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.StatusUpdate.Name = "StatusUpdate";
            this.StatusUpdate.Size = new System.Drawing.Size(501, 532);
            this.StatusUpdate.TabIndex = 20;
            // 
            // CFMPhase2RFIDApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1540, 937);
            this.Controls.Add(this.StatusUpdate);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.OperatorInterface);
            this.Controls.Add(this.Settingsbox);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CFMPhase2RFIDApp";
            this.Text = "CFMPhase2RFIDApp - V1.0";
            this.Load += new System.EventHandler(this.HAASPCHMI_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Settingsbox.ResumeLayout(false);
            this.Settingsbox.PerformLayout();
            this.OperatorInterface.ResumeLayout(false);
            this.OperatorInterface.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Retrievefile;
        private System.Windows.Forms.TextBox PartID;
        private System.Windows.Forms.TextBox ServerLocation_Text;
        private System.Windows.Forms.TextBox Destinationlocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PartIDDefaultSuffix;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton Visionsystem;
        private System.Windows.Forms.RadioButton RFID;
        private System.Windows.Forms.RadioButton Manual;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox IPAddress_Text;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox Settingsbox;
        private System.Windows.Forms.GroupBox OperatorInterface;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label STATUS;
        private System.Windows.Forms.Timer Timer1;
        private System.Windows.Forms.Button STARTTCP;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox FileExtension;
        private System.Windows.Forms.Button RFIDReadbutton;
        private System.Windows.Forms.TextBox Responsetimetext;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer RFIDTimer;
        private System.Windows.Forms.ListBox StatusUpdate;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Timer HeartbeatTimer;
    }
}

