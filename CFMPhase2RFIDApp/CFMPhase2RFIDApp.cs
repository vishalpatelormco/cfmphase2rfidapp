﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using RFIDRead;
using Impinj.OctaneSdk;
using System.Diagnostics;
using AdvancedHMIControls;
using AdvancedHMIDrivers;
using MfgControl.AdvancedHMI.Controls;
using MfgControl.AdvancedHMI.Drivers;
using Newtonsoft.Json;

namespace HAAS_PC_HMI
{
    
    public partial class CFMPhase2RFIDApp : Form
    {
        private EthernetIPforCLX ethernetIPforCFM = new EthernetIPforCLX();
        public RFIDRead.RFIDReadText RFIDReader;
        private string RFIDFeedback;
        public string Visionsysteminputtext = null;
        Stopwatch responsetime = new Stopwatch();
        Stopwatch RFIDresponsetime = new Stopwatch();
        public string lastloadedfile;
        public JsonTextReader jsontext;
        public string ipAddressCFM = "";
        public string partIDFromPLC = "";
        public string readRequestFromPLC = "";
        public string clearRequestFromPLC = "";
        public string heartbeat = "";
        //PLC Tag names
        public string PLCPartID = "";
        public string PLCRequest = "";
        public string PLCPlastic_Type = "";
        public string PLCClearData = "";
        public string PLCHeartbeat = "";
        //Previous clear and request
        public string PrevPLCRequest = "";
        public string PrevPLCClear = "";
        string path = Directory.GetCurrentDirectory();
        private readonly object timerLock = new object();
        public string rollChopper1Mat = "";
        public string rollChopper2Mat = "";
        public string chopper1Comm = "";
        public string chopper2Comm = "";
        public int matLength = 0;
        public string stageType = "";
        public int stageLength = 0;
        public Exception tempException;
        public TimeSpan timeforRFIDresponse;
        public string Metadatafilelocation = "";

        public EventHandler rfidChangeSignalEventHandler;
        private string _readChangeSignal;
        public string ReadChangeSignal
        {
            get { return _readChangeSignal; }
            set
            {
                
                _readChangeSignal = value;
                //rfidChangeSignal();
                //if((_readChangeSignal == "1") && (PrevPLCRequest != _readChangeSignal))
                if (_readChangeSignal == "1")
                 {
                    //this.RFIDTimer.Enabled = false;
                    RFIDReadbutton_Click();
                    PrevPLCRequest = _readChangeSignal;
                    //this.RFIDTimer.Enabled = true;
                }
            }
        }

        private string _clearChangeSignal;
        public string ClearChangeSignal
        {
            get { return _clearChangeSignal; }
            set
            {
                
                _clearChangeSignal = value;
                //rfidChangeSignal();
                //if ((_clearChangeSignal == "1") && (PrevPLCClear != _clearChangeSignal))
                if (_clearChangeSignal == "1" )
                {
                    //this.RFIDTimer.Enabled = false;
                    RFIDReadbutton_Click();
                    PrevPLCClear = _clearChangeSignal;
                    //this.RFIDTimer.Enabled = true;
                }
            }
        }

        //public void rfidChangeSignal()
        //{

        //}


        public CFMPhase2RFIDApp()
        {

            
            InitializeComponent();
            // this.WindowState = FormWindowState.Maximized;

            this.Size = new Size(1298, 675);

            this.RFIDTimer.Enabled = false;

            RFIDReader = new RFIDReadText();
            this.PartID.KeyDown += new KeyEventHandler(PartID_KeyDown);
            STATUS.Width = 500;
            STATUS.Height = 100;
            
            //RFIDReadbutton.Enabled = false;

        }

        private void HAASPCHMI_Load(object sender, EventArgs e)
        {
            try
            {
                

               // StreamReader sr = new StreamReader("C:\\HAASPCHMIFiles\\Configuration.txt");
                StreamReader sr = new StreamReader(path +"\\Configuration.txt");
                //ServerLocation_Text.Text = sr.ReadLine();
                
                string line;
                int counter = 0;
                string defaultmode ="";
                while ((line = sr.ReadLine()) != null)
                {
                    System.Console.WriteLine(line);

                    if (counter == 1)
                    {
                        ServerLocation_Text.Text = line;
                        Metadatafilelocation = line;
                    }
                    if (counter == 3)
                    {
                        PartIDDefaultSuffix.Text = line;
                    }
                    if (counter == 5)
                    {
                        Destinationlocation.Text = line;
                    }
                    if (counter == 7)
                    {
                        IPAddress_Text.Text = line;
                    }
                    if (counter == 9)
                    {
                        FileExtension.Text = line;
                    }
                    if (counter == 11)
                    {
                        defaultmode = line;
                    }
                    if (counter == 13)
                    {
                        ipAddressCFM = line;
                        Console.WriteLine(ipAddressCFM);
                    }
                    if(counter == 15)
                    {
                        PLCPartID = line;
                    }
                    if (counter == 17)
                    {
                        PLCRequest = line;
                    }
                    if (counter == 19)
                    {
                        PLCPlastic_Type = line;
                    }
                    if (counter == 21)
                    {
                        PLCClearData = line;
                        //PLCClearData = "OmrcoRFID.PLCtoPC.ClearReq";
                    }
                    if (counter == 23)
                    {
                        PLCHeartbeat = line;
                    }
                    if(counter == 25)
                    {
                        rollChopper1Mat = line;
                        if(rollChopper1Mat == "zendura")
                        {
                            chopper1Comm = "TruGen";
                        }
                        if (rollChopper1Mat == "XR")
                        {
                            chopper1Comm = "TruGen XR";
                        }
                    }
                    if(counter == 27)
                    {
                        rollChopper2Mat = line;
                        if (rollChopper2Mat == "zendura")
                        {
                            chopper2Comm = "TruGen";
                        }
                        if (rollChopper2Mat == "XR")
                        {
                            chopper2Comm = "TruGen XR";
                        }
                    }
                    counter = counter + 1;
                }


                sr.Close();
                //   Infor for .Net to communicate with Allen Bradley PLC
                //ethernetIPforCFM.IPAddress = "10.248.30.72";
                ipAddressCFM.TrimEnd('\0');
                Console.WriteLine(ipAddressCFM);
                ethernetIPforCFM.IPAddress = ipAddressCFM;
                ethernetIPforCFM.ProcessorSlot = 0;
                ethernetIPforCFM.Port = 44818;
                ethernetIPforCFM.Timeout = 5000;
                Visionsystem.Select();
                Visionsystem.Checked = true;
                STARTTCP.PerformClick();

                Settingsbox.Enabled = false;

                if (defaultmode == "Vision")
                {
                    Visionsystem.Checked = true;
                }
                else if(defaultmode == "RFID")
                {
                    RFID.Checked = true;
                }
                else if(defaultmode == "Manual" )
                {
                    Manual.Checked = true;
                }


                if (Visionsystem.Checked == true)
                {
                    PartID.Enabled = false;
                    STATUS.Text = "TRIGGER CAMERA";
                    
                }
                this.RFIDTimer.Enabled = true;

            }
            catch (Exception f)
            {
                MessageBox.Show("Exception");
                Console.WriteLine("Exception: " + f.Message);
                MessageBox.Show(f.Message);
            }
        }


        private void PartID_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }



        private void PartID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Retrievefile.PerformClick();
                // these last two lines will stop the beep sound
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
        }

        private void Retrievefile_Click(object sender, EventArgs e)
        {
            TimeSpan ts;
            ts = TimeSpan.Zero;

            responsetime.Reset();

            responsetime.Start();
            Responsetimetext.Text = "";

            Retrievefile.Enabled = false;

            string selectedpath = ServerLocation_Text.Text;
            string completefilelocation;
            string completefilename;
            string filelocationwithcasefolder;

            string fileName;

            //string s12;
            //s12 = "C:\\Gcode\\";
            //ClearAttributes(s12);

            

            System.IO.DirectoryInfo di = new DirectoryInfo(Destinationlocation.Text.ToString());

            foreach (FileInfo file in di.GetFiles())
            {
                try
                { 
                
                file.Delete();
                }

                catch (Exception f)
                {
                    // Console.WriteLine("Exception: " + f.Message);
                    goto V;
                }
                V:
                continue;
                
                
            }

            

          

           
            try
            {
             

          





                string fileextension = FileExtension.Text;

                //string fileextension = ".trm";

                string s1 = null;
                s1 = PartID.Text;
                string s2 = null;
                int pos = 0;

                


                if(s1 == lastloadedfile)
                {
                    Timer1.Start();
                    return;
                    
                }


                Boolean ContainsError = s1.Contains("ERR");

                
                 
                  if (ContainsError == true)
                    {
                        STATUS.Text = "ERROR, PLEASE Enter Manually";
                        Retrievefile.Enabled = true;
                        PartID.Enabled = true;
                        PartID.Text = "";
                        recorddata(s1.ToString(), "");
                    return;
                    }



                if (s1.IndexOf('L') != -1)
                {
                    pos = s1.IndexOf("L");
                }
                else if (s1.IndexOf('U') != -1)
                {
                    pos = s1.IndexOf("U");
                }
                else if (s1.IndexOf('l') != -1)
                {
                    pos = s1.IndexOf("l");
                }
                else if (s1.IndexOf('u') != -1)
                {
                    pos = s1.IndexOf("u");
                }
                else
                {
                    if (RFID.Checked == true)
                    {
                        return;
                    }
                    MessageBox.Show("CHECK THE PART AND TRY AGAIN, NO L OR U FOUND");
                    Retrievefile.Enabled = true;
                    PartID.Text = "";
                    recorddata(s1.ToString(), "");
                    
                    return;
                }
                s2 = s1.Substring(0, pos);


                filelocationwithcasefolder = selectedpath + s2 + "\\" + "Gcodes" + "\\";

                Console.WriteLine("Exception: " + filelocationwithcasefolder);
                Console.WriteLine("Testing :" + s1);

                if (RFID.Checked == true)
                {
                    completefilename = PartID.Text + fileextension;
                }
                else
                {
                    completefilename = s1 + PartIDDefaultSuffix.Text + fileextension;
                }

                Console.WriteLine("CompleteFilename :" + completefilename);
                Console.WriteLine("TImer " + responsetime.ElapsedMilliseconds.ToString());

                string[] files = Directory.GetFiles(filelocationwithcasefolder.ToString(), completefilename, SearchOption.AllDirectories);


                Console.WriteLine("TImer " + responsetime.ElapsedMilliseconds.ToString());

                foreach (string s in files)
                {
                    // Use static Path methods to extract only the file name from the path.
                    fileName = System.IO.Path.GetFileName(s);
                    string destFile = System.IO.Path.Combine(Destinationlocation.Text.ToString(), fileName);
                    System.IO.File.Copy(s, destFile, true);
                }

                Console.WriteLine("TImer " + responsetime.ElapsedMilliseconds.ToString());

                completefilelocation = files[0];

                //  System.Windows.Forms.MessageBox.Show(completefilelocation);

               // string value = File.ReadAllText(completefilelocation);


                if (Manual.Checked == true)
                {


                    STATUS.Text = "File Transfer Complete";
                    PartID.Text = "";
                }
                if (Visionsystem.Checked == true)
                {
                    STATUS.Text = "File Transfer Complete";

                    PartID.Text = "";
                }

                if (RFID.Checked == true)
                {
                    STATUS.Text = "File Transfer Complete";

                    PartID.Text = "";

                    lastloadedfile = s1;
                }

                Console.WriteLine("Before TImer Starts");

                Timer1.Start();

                Console.WriteLine("Timer Completed");

                if (Manual.Checked == true)
                {
                    Retrievefile.Enabled = true;
                }

               
                responsetime.Stop();

                //ts = responsetime.ElapsedMilliseconds.ToString();

                Responsetimetext.Text = responsetime.ElapsedMilliseconds.ToString();

                recorddata(s1, responsetime.ElapsedMilliseconds.ToString());
                updatestatusbox(s1);

            }
            catch (Exception f)
            {
                Console.WriteLine("Exception: " + f.Message);

                if (f.Message == "Index was outside the bounds of the array.")
                {
                    if (RFID.Checked == true)
                    {
                        
                        updatestatusbox("File Not Found, please check file exists in Server location");
                        Timer1.Start();
                        return;
                    }

                    MessageBox.Show("File Not Found, please check file exists in Server location");
                    PartID.Text = "";
                }
                else if (f.Message.Substring(0, 15) == "Could not find ")
                {
                    if (RFID.Checked == true)
                    {
                        
                        updatestatusbox("Patient Folder do not exist in Server location");
                        Timer1.Start();
                        return;
                    }
                    MessageBox.Show("Patient Folder do not exist in Server location");
                    PartID.Text = "";
                }
                else
                {

                    if (RFID.Checked == true)
                    {

                        updatestatusbox(f.Message);
                        Timer1.Start();
                        return;
                    }
                    MessageBox.Show(f.Message);
                    PartID.Text = "";
                }

                if (Manual.Checked == true)
                {
                    Retrievefile.Enabled = true;
                }

               
            }

           
        }


        public static string ToReadableString(TimeSpan span)
        {
            string formatted = string.Format("{0}{1}{2}{3}",
                span.Duration().Days > 0 ? string.Format("{0:0} day{1}, ", span.Days, span.Days == 1 ? String.Empty : "s") : string.Empty,
                span.Duration().Hours > 0 ? string.Format("{0:0} hour{1}, ", span.Hours, span.Hours == 1 ? String.Empty : "s") : string.Empty,
                span.Duration().Minutes > 0 ? string.Format("{0:0} minute{1}, ", span.Minutes, span.Minutes == 1 ? String.Empty : "s") : string.Empty,
                span.Duration().Seconds > 0 ? string.Format("{0:0} second{1}", span.Seconds, span.Seconds == 1 ? String.Empty : "s") : string.Empty);

            if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

            if (string.IsNullOrEmpty(formatted)) formatted = "0 seconds";

            return formatted;
        }

        private void CallServer()
        {

            try
            {


                byte[] buffer = new byte[1000];
                byte[] msg = Encoding.ASCII.GetBytes("From server\n");
                string data = null;
                string ipstring = null;
                ipstring = IPAddress_Text.Text;




                IPHostEntry iphostInfo = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress ipAddress = iphostInfo.AddressList[0];

                //IPAddress ipAddress = IPAddress.Parse(ipstring);

                //IPAddress ipAddress = IPAddress.Parse("169.254.158.152");

                IPEndPoint localEndpoint = new IPEndPoint(ipAddress, 3000);



                //MessageBox.Show(localEndpoint.ToString);

                ConsoleKeyInfo key;
                int count = 0;


                Socket sock = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                MessageBox.Show("Right before the problem 1");

                sock.Bind(localEndpoint);

                MessageBox.Show("Right before the problem 2");

                sock.Listen(5);

                MessageBox.Show("Listening to the socket");

                while (true)
                {

                    Console.WriteLine("\nWaiting for clients..{0}", count);
                    Socket confd = sock.Accept();



                    int b = confd.Receive(buffer);
                    data += Encoding.ASCII.GetString(buffer, 0, b);

                    Boolean ContainsError = data.Contains("ERROR");

                    Invoke(new Action(() =>
                    {
                        if (ContainsError == false)
                        {
                            PartID.Text = data.ToString();
                            STATUS.Text = "In Process";
                            Retrievefile.Enabled = true;
                            Retrievefile.PerformClick();
                        }
                        else if (ContainsError == true)
                        {
                            STATUS.Text = "ERROR, PLEASE Enter Manually";
                            Retrievefile.Enabled = true;
                            PartID.Enabled = true;

                            recorddata(data.ToString(), "");
                        }
                    // Thread.Sleep(10000);

                }));



                    Console.WriteLine("" + data);
                    data = null;

                    confd.Send(msg);

                    confd.Close();
                    count++;

                    Application.DoEvents();

                }

            }

            catch (Exception f)
            {
                Console.WriteLine("Exception: " + f.Message);
                MessageBox.Show(f.Message);
            }



        }



        private void Timer1_Tick(object sender, EventArgs e)
        {
            if (Visionsystem.Checked == true)
            {
                STATUS.Text = "Trigger Camera";
                PartID.Enabled = false;
                Retrievefile.Enabled = false;
                PartID.Text = "";
            }
            if (Manual.Checked == true)
            {
                STATUS.Text = "Manual Mode-Enter Part ID";
                PartID.Enabled = true;
                Retrievefile.Enabled = true;
                PartID.Text = "";
            }
            if (RFID.Checked == true)
            {
                STATUS.Text = "RFID Waiting";
                PartID.Enabled = false;
                PartID.Text = "";

            }

            PartID.Text = "";
            Timer1.Stop();
        }


        public static void ClearAttributes(string currentDir)
        {
            if (Directory.Exists(currentDir))
            {
                string[] subDirs = Directory.GetDirectories(currentDir);
                foreach (string dir in subDirs)
                    ClearAttributes(dir);
                string[] files = files = Directory.GetFiles(currentDir);
                foreach (string file in files)
                    File.SetAttributes(file, FileAttributes.Normal);
            }
        }

        private void STARTTCP_Click(object sender, EventArgs e)
        {
            //            CallServer();
            STARTTCP.BackColor = Color.Green;

            //Thread CallServerinParallel = new Thread(CallServer);
            // CallServerinParallel.Start();
            Thread CallScanninginParallel = new Thread(ScanningforInput);
            CallScanninginParallel.Start();
        }

        private void Manual_CheckedChanged(object sender, EventArgs e)
        {
            PartID.Enabled = true;
            STATUS.Text = "Manual Mode-Enter PartID";
            Retrievefile.Enabled = true;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void RFID_CheckedChanged(object sender, EventArgs e)
        {
            STATUS.Text = "RFID Waiting";
            PartID.Enabled = false;
            Retrievefile.Enabled = false;

            ConnectRFIDReader();
            Timer1.Start();


        }

        private void Visionsystem_CheckedChanged(object sender, EventArgs e)
        {
            PartID.Enabled = false;
            Retrievefile.Enabled = false;
            STATUS.Text = "Trigger Camera";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Retrievefile.Enabled = true;
            Retrievefile.PerformClick();
        }


        private void recorddata(string s, string ftptime)
        {
            string s1, s2, s3;
            try
            {
                string date = DateTime.Now.ToString("yyyy/M/dd ");
                string time = DateTime.Now.ToString("hh:mm:ss");
                s2 = Convert.ToDateTime(date + time).ToString("yyyy/MM/dd HH:mm:ss");
                s3 = "";
                if (Visionsystem.Checked == true)
                {
                    s3 = "Vision System";
                }
                else if (Manual.Checked == true)
                {
                    s3 = "Manual Entry";
                }
                else if (RFID.Checked == true)
                {
                    s3 = "RFID Entry";
                }

                s1 = s2 + " " + s + " " + s3 + " " + ftptime;


                StreamWriter srwrite = new StreamWriter(path + "\\RecordedData.txt", true);

                srwrite.WriteLine(s1);

                srwrite.Close();


                Settingsbox.Enabled = false;


            }
            catch (Exception f)
            {
                Console.WriteLine("Exception: " + f.Message);
                MessageBox.Show(f.Message);
            }

        }

        private void updatestatusbox(string s)
        {
            string s1, s2, s3;
           
                string date = DateTime.Now.ToString("yyyy/M/dd ");
                string time = DateTime.Now.ToString("hh:mm:ss");
                s2 = Convert.ToDateTime(date + time).ToString("yyyy/MM/dd HH:mm:ss");
                s3 = "";
               

                s1 = s2 + " " + s + " " + s3;
            //Following Items.Add was working fine. But the list box needs to display in reverse. 
                StatusUpdate.Items.Add(s1);

            // Try Following 
            StatusUpdate.Items.Insert(0, s1);

        }


        private void ConnectRFIDReader()
        {

            string isreaderconnected = null;

            string Readerhostname = null;

            try
            {
                StreamReader sr = new StreamReader(path + "\\Readerhostname.txt");
                Readerhostname = sr.ReadLine();
                sr.Close();


                isreaderconnected = RFIDReader.Connectreader(Readerhostname);

                if (isreaderconnected == "Readerconnected")
                {
                    RFIDReadbutton.BackColor = Color.Green;
                }
                else
                {
                    RFIDReadbutton.BackColor = Color.Red;
                }


            }
            catch (Exception f)
            {
                Console.WriteLine("Exception: " + f.Message);
                MessageBox.Show(f.Message);
                MessageBox.Show("Change for pullpush");
            }


  

        }


        private void RFIDReadbutton_Click()
        {
            clearRequestFromPLC = ethernetIPforCFM.Read(PLCClearData);
            if (clearRequestFromPLC == "1") 
            {
                ethernetIPforCFM.Write(PLCPlastic_Type, "");
                ethernetIPforCFM.Write(PLCPartID, "");
                
            }
            readRequestFromPLC = ethernetIPforCFM.Read(PLCRequest);
            if (readRequestFromPLC == "1")
            {
                //As soon as I get the request from PLC RFID time should start counting
                RFIDresponsetime.Reset();
                RFIDresponsetime.Start();
                try
                {

                    //Txt_BarcodeData1.Text = "";
                    RFIDFeedback = null;

                    if (RFIDReadbutton.BackColor.Equals(Color.Red))
                    {
                        ConnectRFIDReader();
                        return;
                    }

                    // Txt_BarcodeData1.Text = RFIDReader.ReadRFID();


                    RFIDFeedback = RFIDReader.ReadRFID();
                    StatusUpdate.Items.Insert(0, RFIDresponsetime.ElapsedMilliseconds.ToString());





                //    if (RFIDFeedback == "")
                //    {
                //        RFIDFeedback = RFIDReader.ReadRFID();
                //        StatusUpdate.Items.Insert(0, "Second Attempt" + RFIDresponsetime.ElapsedMilliseconds.ToString());
                //    }
                 //   if (RFIDFeedback == "")
                //    {
                 //       RFIDFeedback = RFIDReader.ReadRFID();
                //        StatusUpdate.Items.Insert(0, "Third Attempt" + RFIDresponsetime.ElapsedMilliseconds.ToString());
                //    }

               

                    string s1 = null;
                    s1 = RFIDFeedback.Trim();

                    s1 = s1.TrimEnd('\0');
                    string s2 = null;
                    int pos = 0;

                    // write rfid part number for PLC
                    ethernetIPforCFM.Write(PLCPartID, s1);
                    Console.WriteLine("PLCPartID :" + s1);
                    StatusUpdate.Items.Insert(0, "PLCPartID :" + s1);

                    //file name for aligner metadata
                    string metaDataName = null;
                    int indexOfL = s1.IndexOf('L');
                    int indexOfU = s1.IndexOf('U');
                    //string path = Directory.GetCurrentDirectory();
                    //metaDataName = path + "\\" + s1.Substring(0, 6) + "_Aligner_Metadata_File.json";
                    if (indexOfL != -1)
                    {
                        metaDataName = Metadatafilelocation + "\\" + s1.Substring(0, indexOfL) + "_Aligner_Metadata_File.json";
                        Console.WriteLine(indexOfL);
                    }
                    if (indexOfU != -1)
                    {
                        metaDataName = Metadatafilelocation + "\\" + s1.Substring(0, indexOfU) + "_Aligner_Metadata_File.json";
                        Console.WriteLine(indexOfU);
                    }
                    string materialType = " ";
                    string readMaterial = " ";
                    var jsonText = File.ReadAllText(metaDataName);
                    //var molds = JsonConvert.DeserializeObject(jsonText);
                    int counter = 0;
                    string line;
                    System.IO.StreamReader file = new System.IO.StreamReader(metaDataName);
                    while ((line = file.ReadLine()) != null)
                    {
                        System.Console.WriteLine(line);
                        counter++;
                        if (line.Contains("type"))
                        {
                            stageType = line.Substring(line.IndexOf(':') + 3);
                            stageLength = stageType.Length;
                            stageType = stageType.Substring(0, stageLength - 2);
                        }
                        if (line.Contains(s1))
                        {
                            // System.Console.WriteLine("Found it!!!!!!!!!!!!!!!!!!!!");
                            while ((line = file.ReadLine()) != null)
                            {
                                if(stageType == "NOTHINGNEEDTOBYPASS")
                                {
                                    materialType = "copyplast,3";
                                    if (materialType != null)
                                    {
                                        System.Console.WriteLine(materialType);
                                        ethernetIPforCFM.Write(PLCPlastic_Type, materialType);
                                        readMaterial = ethernetIPforCFM.Read(PLCPlastic_Type);
                                        partIDFromPLC = ethernetIPforCFM.Read(PLCPartID);
                                        textBox1.Text = partIDFromPLC;
                                        if (materialType != readMaterial)
                                        {
                                            Console.WriteLine("Mismatch in sent data");
                                        }
                                        break;
                                    }

                                }
                                if (line.Contains("material_type"))
                                {

                                    materialType = line.Substring(line.IndexOf(':') + 3);
                                    matLength = materialType.Length;
                                    materialType = materialType.Substring(0, matLength - 1);
                                    if (materialType == chopper1Comm)
                                    {
                                        //materialType =  materialType.Substring(0, matLength-1 ) + ",1";
                                        materialType = rollChopper1Mat + ",1";
                                    }
                                    if (materialType == chopper2Comm)
                                    {
                                        //materialType = materialType.Substring(0, matLength -1) + ",2";
                                        materialType = rollChopper2Mat + ",2";
                                    }
                                    if (materialType == "copyplast")
                                    {
                                        materialType = materialType.Substring(0, matLength -1) + ", 1";
                                    }


                                   
                                        
                                    if (materialType != null)
                                    {
                                        System.Console.WriteLine(materialType);
                                        ethernetIPforCFM.Write(PLCPlastic_Type, materialType);


                                        Console.WriteLine("PLC Plastic Type sent : " + materialType);
                                        StatusUpdate.Items.Insert(0, "PLC Plastic Type sent : " + materialType);


                                        readMaterial = ethernetIPforCFM.Read(PLCPlastic_Type);

                                        Console.WriteLine("PLC Plastic Type read : " + readMaterial);
                                        StatusUpdate.Items.Insert(0, "PLC Plastic Type read : " + readMaterial);

                                        partIDFromPLC = ethernetIPforCFM.Read(PLCPartID);
                                        textBox1.Text = partIDFromPLC;

                                        Console.WriteLine("PartID from PLC : " + partIDFromPLC);
                                        StatusUpdate.Items.Insert(0, "PartID from PLC : " + partIDFromPLC);


                                        RFIDresponsetime.Stop();

                                        Console.WriteLine("Timer - PLC request to read to material type end" + RFIDresponsetime.ElapsedMilliseconds.ToString());
                                        StatusUpdate.Items.Insert(0, "Timer - PLC request to read to material type end" + RFIDresponsetime.ElapsedMilliseconds.ToString());

                                        if (materialType != readMaterial)
                                        {
                                            Console.WriteLine("Mismatch in sent data");
                                        }
                                        break;
                                    }
                                }

                            }
                        }
                        // System.Console.WriteLine(materialType);
                    }
                    file.Close();
                    

                    // Suspend the screen.  
                    System.Console.ReadLine();

                    //var molds = JsonConvert.DeserializeObject<List<Molds>>(jsonText);
                    //var first = molds[1];

                    //var moldsById = molds.toDictionary(X => X.material_type);
                    //var indexOfMold = molds.indexOf(s1);
                    //string materialType = molds.

                    // jsontext = new JsonTextReader(new StringReader(metaDataName));
                    // while (jsontext.Read())
                    // {
                    //     if (jsontext.Value != null)
                    //     {
                    //        if( jsontext.TokenType == JsonToken.PropertyName && jsontext.Value.ToString() == "id_tag")
                    //         {
                    //             ;
                    //        }
                    //     }
                    // }



                    if (s1.IndexOf('L') != -1)
                    {
                        pos = s1.IndexOf("L");
                    }
                    else if (s1.IndexOf('U') != -1)
                    {
                        pos = s1.IndexOf("U");
                    }
                    else
                    {
                        //MessageBox.Show("CHECK THE PART AND TRY AGAIN, NO L OR U FOUND");
                        recorddata(s1, "Check the part and try again, NO L OR U FOUND");
                        return;
                    }

                    //s2 = s1.Insert(pos, "-");
                    //string s5 = "RFIDRead" + s2;

                    // recorddata(s1,"Incorrect Typed PartID");

                    PartID.Text = s1;
                    STATUS.Text = s1;
                    Retrievefile.Enabled = true;
                    Retrievefile.PerformClick();



                }

                catch (Exception err)
                {

                    // MessageBox.Show("RFID Read Button Method " + err.Message);
              
                    StatusUpdate.Items.Insert(0,err.ToString());
               //     StatusUpdate.Items.Insert()
                }
            }

        }

        private void RFIDReadbutton_Click(object sender, EventArgs e)
        {
            RFIDReadbutton_Click();

        }

        private void button1_Click(object sender, EventArgs e)
        {
         Process.GetCurrentProcess().Kill();
         Application.ExitThread();
         Application.Exit();
           
        }

        private void ScanningforInput()
        {
            X:

            while (true)
            { 
                try
                {

                    StreamReader sr = new StreamReader("C:\\FTPCognex\\CaseIDforTrimmer.txt");
                    //ServerLocation_Text.Text = sr.ReadLine();
                    string line;
                    line = "";
                    int counter = 0;
                    line = sr.ReadLine();
                    sr.Close();

                   

                    Thread.Sleep(1000);

                    System.IO.File.Delete("C:\\FTPCognex\\CaseIDforTrimmer.txt");

                    Thread.Sleep(100);

                    if (line != "")
                    { 

                    Invoke(new Action(() =>
                    {
                        PartID.Enabled = true;
                        PartID.Text = line;
                        Retrievefile.Enabled = true;
                        Application.DoEvents();
                        Console.WriteLine("PerformClick");
                        Retrievefile.PerformClick();
                        Console.WriteLine("PerformClickCompleted");
                    }));


                    }



                }

                catch
                {
                    Thread.Sleep(100);
                    goto X;
                }

            }
        }

    
       

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;




            // Confirm user wants to close
            switch (MessageBox.Show(this, "Are you sure you want to close?", "Closing", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                default:
                    break;
            }
          Process.GetCurrentProcess().Kill();
          Application.ExitThread();
          Application.Exit();


        }

        private void RFIDTimer_Tick(object sender, EventArgs e)
        {
            ClearChangeSignal = ethernetIPforCFM.Read(PLCClearData);
            ReadChangeSignal = ethernetIPforCFM.Read(PLCRequest);
            //readRequestFromPLC = ethernetIPforCFM.Read(PLCRequest);
            //clearRequestFromPLC = ethernetIPforCFM.Read(PLCClearData);
            //if ((PrevPLCClear != clearRequestFromPLC) || (PrevPLCRequest != readRequestFromPLC))
            //{
            //    lastloadedfile = "";
            //    RFIDReadbutton_Click();
            //}
            //PrevPLCClear = clearRequestFromPLC;
            //PrevPLCRequest = readRequestFromPLC;
        }

        private void HeartbeatTimer_Tick(object sender, EventArgs e)
        {
            

            try
            { 
                            
                lock (timerLock)

                {
                    ethernetIPforCFM.Write(PLCHeartbeat, 1);
                    //wait(500);
                    System.Threading.Thread.Sleep(500);
                    ethernetIPforCFM.Write(PLCHeartbeat, 0);
                }

            }
            catch 
            {
                StatusUpdate.Items.Insert(0, "Not Connected to PLC");
            }

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        public void wait(int milliseconds)
        {
            var timer1 = new System.Windows.Forms.Timer();
            if (milliseconds == 0 || milliseconds < 0) return;

            // Console.WriteLine("start wait timer");
            timer1.Interval = milliseconds;
            timer1.Enabled = true;
            timer1.Start();

            timer1.Tick += (s, e) =>
            {
                timer1.Enabled = false;
                timer1.Stop();
                // Console.WriteLine("stop wait timer");
            };

            while (timer1.Enabled)
            {
                Application.DoEvents();
            }
        }
    }
}

